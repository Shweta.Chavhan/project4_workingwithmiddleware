//Middleware is the code which will be performed first before request goes to its specific route
/*
Before using middleware
request------> Server------> To requested route
After using middleware
request ------> Server-------> Middleware------> TO requested route

app.use() is used to define middleware
We can use app.use() as many time as we want. It will create multiple middleware.
There is no point in having multiple middleware untill and unless they are coming from passwordjs

In some of the cases we may need to grab cookies from header or user authentication. And thats the use of middleware
we can do this using various URL class methods

Most of the times we dont need to write most of the middleware. They comes in built in passport js, firebase module
*/

const express = require("express");
const app = express();

const port = 9090;
const hostname = "127.0.0.1";

var mymiddleware = function(req, res, next) {
  console.log("I am MIDDLEWARE");
  next();
};

//To show server time
var serevrtime = function(req, res, next) {
  req.requestTime = new Date();
  next();
};

app.use(serevrtime); //we can pass name of middleware which we want to use here if we want to use only single middleware. In case of multiple middleware we will have app.use

app.listen(port, hostname, () => {
  console.log(`server is running at ${hostname}:${port}`);
});

app.get("/", (req, res) => {
  res.send("Welcome.."); //The variable defined inside middleware is accessible here also
  console.log("Hello from / The request time is : " + req.requestTime);
});
